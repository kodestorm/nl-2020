/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jones.dao.bean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import nl.jones.dao.UsersDao;
import nl.jones.entity.Users;

@ManagedBean(name = "LoginBean")
@ViewScoped
public class LoginManagedBean {

    private UsersDao usuarioDAO = new UsersDao();

    private Users user = new Users();

    public String login() {

        user = usuarioDAO.getUser(user.getName(), user.getPassword());
        if (user == null) {
            user = new Users();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!", "Erro no Login!"));
            return null;
        } else {
            return "/faces/dashboard.xhtml";
        }

    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
