/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jones.dao.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import nl.jones.dao.UsersDao;
import nl.jones.dao.exceptions.NonexistentEntityException;
import nl.jones.entity.Users;

/**
 *
 * @author kodestorm
 */
@ManagedBean(name = "usersBean")

@ViewScoped
public class UsersManagedBean implements Serializable {

    private List<Users> users;

    private Users userNew = new Users();

    private Users userEdit;

    @Inject
    private UsersDao service;

    @PostConstruct
    public void init() {
        users = service.getAll();

        ExternalContext ec2 = FacesContext.getCurrentInstance().getExternalContext();
        Users usr = (Users) ec2.getRequestMap().get("userEdit");
        if (usr != null) {
            this.setUserEdit(usr);
        }

    }

    public Users getUserEdit() {
        return userEdit;
    }

    public void setUserEdit(Users userEdit) {
        this.userEdit = userEdit;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setService(UsersDao service) {
        this.service = service;
    }

    public Users getUserNew() {
        return userNew;
    }

    public void setUserNew(Users userNew) {
        this.userNew = userNew;
    }

    public String remove(Integer id) throws NonexistentEntityException {
        this.service.destroy(id);
        return "/faces/userList.xhtml";
    }

    public String edit(Integer id) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.getRequestMap().put("userEdit", service.findUsers(id));

        return "/faces/userEdit.xhtml";
    }

    public String save() throws Exception {
        Users usr = new Users(null, userNew.getName(), userNew.getEmail(), userNew.getPassword());
        this.service.create(usr);
        this.userNew = new Users();
        return "/faces/userList.xhtml";
    }

    public String update() throws Exception {
        Users userUpdate = this.service.findUsers(userEdit.getId());
        userUpdate.setName(userEdit.getName());
        userUpdate.setEmail(userEdit.getEmail());
        this.service.edit(userUpdate);
        return "/faces/userList.xhtml";
    }

}
